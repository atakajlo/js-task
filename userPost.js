class UserPost {
    constructor(userId, title, body) {
        this.userId = userId;
        this.title = title;
        this.body = body;

    }

    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) {
            element.textContent = text;
        }
        element.classList.add(...classNames);
        return element
    }

    renderPost() {
            this.divPosts = this.createElement("div", ["list-group-item"])
            this.divPosts.insertAdjacentHTML("afterbegin", `<h3>${this.title}</h3><p>${this.body}</p>`);
            return this.divPosts
    }
       

    render() {
        const userPost = this.createElement("div", ["card", "user-post"])
        userPost.append(this.renderPost());
        return userPost
    }

}

export default UserPost