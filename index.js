import UserCard from './userCard.js';
import UserPost from "./userPost.js";



const tasksBoard = document.querySelector(".tasks-board");

async function displayUsers() {
    const response = await fetch(`https://ajax.test-danit.com/api/json/users/`)
    const users = await response.json();

    for (const user of users) {
        const {name, username, email, id} = user;
        const cardUser = new UserCard(name, username, email, id)

        const response = await fetch(`https://ajax.test-danit.com/api/json/posts`)
        const posts = await response.json();
        posts.forEach(post => {
            const {userId, title, body} = post;
            if (cardUser.id === userId) {
                const cardPost = new UserPost(userId, title, body)
                console.log(cardPost);
                cardUser.addPost(cardPost);
            }
        })

        tasksBoard.append(cardUser.render());
        console.log(cardUser);


        // cardUser.addPost(cardPost)
        // cartUser.renderPost()
    }


}

displayUsers()